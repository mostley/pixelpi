import time, random
from math import *

class Underlight:

	def __init__(self):
		self.rgb = RGB()

		self.running = True

		self.t = 0
		
		self.spidev = file("/dev/spidev0.0", "w")


	def update(self):
		self.t = (math.sin(self.time)/2) + 0.5
	
	def getColor(self):
		return (200 + self.t, 0, 0)

	def draw(self):
		data = bytearray([0 for i in range(125 * 3)]
		
		for i in range(8):
			data[125 + i] = self.getColor()
			
		self.spidev.write(data))
		self.spidev.flush()

	def run(self):
		while self.running:
			self.update()
			self.draw()

if __name__ == "__main__":
	heartbeat = Underlight()
	heartbeat.run()