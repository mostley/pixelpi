#!/usr/bin/python
# -*- coding: utf8 -*-

import time, subprocess
from evdev import *

class ControllCenter:
	
	def __init__(self):
		self.lastFrame = 0
		self.keyboard = Device("/dev/input/event0")
		self.modeFolder = "/home/pi/pixelpi/controllcenter/modes/"
		
		self.modes = [ 'heartbeat.py', 'snake.py', 'underlight.py' ]
		self.currentModeIndex = -1
		self.currentMode = None

	def execute(self, cmd):
		print "Executing ", cmd
		
		if cmd == "off":
			self.currentModeIndex = -1
			self.stopCurrentMode()
			
			spidev = file("/dev/spidev0.0", "w")
			spidev.write(bytearray([0 for i in range(125 * 3)]))
			spidev.flush()
			
		elif cmd == "switchMode":
			lastMode = self.currentModeIndex
			if self.currentModeIndex == -1:
				self.currentModeIndex = 0
			else:
				self.currentModeIndex += 1
			
			if self.currentModeIndex >= len(self.modes):
				self.currentModeIndex = len(self.modes) - 1
			
			if lastMode != self.currentModeIndex:
				self.stopCurrentMode()
				self.startCurrentMode()
	
	def stopCurrentMode(self):
		if self.currentMode:
			self.currentMode.kill()
			self.currentMode = None
	
	def startCurrentMode(self):
		if self.currentModeIndex >= 0:
			print "Start Mode ", self.modes[self.currentModeIndex]
			filename = self.modeFolder + self.modes[self.currentModeIndex]
			self.currentMode = subprocess.Popen(["/usr/bin/python", filename])

	def handleInput(self):
		if self.getKey("KEY_F12"):
			self.execute("off")
		elif self.getKey("KEY_F11"):
			self.execute("switchMode")
	
	def getKey(self, key):
		 return key in self.keyboard.buttons and self.keyboard.buttons[key]

	def run(self):
		while True:
			self.lastFrame = time.time()
			
			self.handleInput()
			
			while (time.time() - self.lastFrame) < 0.1:
				self.keyboard.poll()
				time.sleep(0.2)
			                
if __name__ == "__main__":
	print "Starting Controll Center"
	center = ControllCenter()
	center.run()
	print "Controll Center is shutting down"