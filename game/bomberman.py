#!/usr/bin/python
# -*- coding: utf8 -*- 

from evdev import *
from librgb import *
import time, random, os

COLORS = [BLACK, BLUE, RED, YELLOW, PURPLE, GREEN, ORANGE, TURQUE]

class Keys:
	Left = 0
	Right = 1
	Up = 2
	Down = 3
	Bomb = 4

class Main:
    def __init__(self):
        self.rgb = RGB()#"127.0.0.1")# "192.168.1.5")
        self.rgb.invertedX = True
        self.rgb.invertedY = False
        self.rgb.verbose = False

        self.isOff = False
        self.keyboard = Device("/dev/input/event0")
        self.characters = [Vector(0,0), Vector(0,0), Vector(0,0), Vector(0,0)]
        self.lastFrame = time.time()

        self.backgroundColor = BLACK

        self.table = self.createTable()

        self.gameLost = False

        self.musicFiles = []
        self.initMusic()
        
        self.keymapping = {
        	0: {
        		Keys.Left: "KEY_LEFT",
        		Keys.Right: "KEY_RIGHT",
        		Keys.Up: "KEY_UP",
        		Keys.Down: "KEY_DOWN",
        		Keys.Bomb: "KEY_SPACE"
        	}
    	}

    def initMusic(self):
        musicDir = "music/"
        if not os.path.exists(musicDir):
            print "no music dir (",musicDir,")"
        else:
            files = os.listdir(musicDir)

            for f in files:
                if f.endswith(".mp3"):
                    self.musicFiles.append(musicDir + f)

            if len(self.musicFiles) == 0:
                print "no music files in dir (",musicDir,")"
            
            self.playNextSong()

    def playNextSong(self):
        if len(self.musicFiles) > 0:
            songFile = random.choice(self.musicFiles)
            
            print "play song ",songFile
            os.system('mpg321 "' + songFile + '" &')

    def createTable(self):
        return [[0 for i in range(PIXEL_DIM_X)] for i in range(PIXEL_DIM_Y)]

    def getKey(self, key):
        return key in self.keyboard.buttons and self.keyboard.buttons[key]

    def handleInput(self):
        if self.currentShape:
            if self.getKey("KEY_LEFT"):
                self.currentShape.moveLeft(self.table)
            if self.getKey("KEY_RIGHT"):
                self.currentShape.moveRight(self.table)
            if self.getKey("KEY_DOWN"):
                self.isDroppingFast = True
            if self.getKey("KEY_UP"):
                self.currentShape.rotate(self.table)
        if self.getKey("KEY_SPACE"):
            print "new game"
            if self.gameLost:
                self.gameLost = False
                self.table = self.createTable()
                self.currentDropInterval = self.startDropInterval
            else:
                self.playNextSong()
        if self.getKey("KEY_ESC"):
            self.isOff = not self.isOff

    def update(self):
        if self.gameLost:
            done = False
            for x in range(len(self.table[0])):
                for y in range(len(self.table)):
                    if self.table[y][x] == 0:
                        self.table[y][x] = random.randrange(1, len(COLORS))
                        done = True
                        break
                if done: break
        else:#if (time.time() - self.lastShape) < self.shapeInterval:
            self.currentShape = random.choice(self.shapes)
            self.currentShape.colorIndex = random.randrange(1,len(COLORS))
            self.currentShape.position = Vector(PIXEL_DIM_X - 1, PIXEL_DIM_Y/2)
            self.currentShape.rotateRandom()

            if not self.currentShape.canBePlaced(self.table):
                self.gameOver()

    def gameOver(self):
        self.gameLost = True

    def draw(self):
        for y in range(PIXEL_DIM_Y):
            for x in range(PIXEL_DIM_X):
                if self.table[y][x] != 0:
                    c = self.table[y][x]
                    self.rgb.setPixel(Vector(x,y), COLORS[c])

        if self.currentShape:
            self.currentShape.draw(self.rgb)

    def run(self):
        while True:
            self.lastFrame = time.time()
            self.rgb.clear(self.backgroundColor)

            self.handleInput()
            
            if not self.isOff:
                self.update()

                self.draw()

            self.rgb.send()
            
            while (time.time() - self.lastFrame) < 0.1:
                self.keyboard.poll()
                #time.sleep(0.4)

if __name__ == "__main__":
    main = Main()
    main.run()


